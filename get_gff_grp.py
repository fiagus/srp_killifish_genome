import pandas as pd, numpy as np

gff = pd.read_csv('samples/exonerate/all_human_exonerate.gff',sep='\t',header=None)
gff[9] = [ _.split(';')[1] for _ in gff[8] ]
gff[10] = [ 'Parent' if 'Parent' in _.split(';')[0] else 'ID' for _ in gff[8] ]
gff[11] = np.nan

seq = gff.iloc[0,9]
for i in range(len(gff)):
    if 'sequence' in gff.iloc[i,9]:
        gff.iloc[i,11] = gff.iloc[i,9]
        seq = gff.iloc[i,9]
    else:
        gff.iloc[i,11] = seq

gff.to_csv('samples/exonerate/all_human_exo_grp.gff',index=False,sep='\t',header=None)
