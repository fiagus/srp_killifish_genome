library(GEOquery)

gse <- getGEO("GSE156460", GSEMatrix = TRUE)
#df <- data.frame(pData(phenoData(gse[[1]]))[c(1,2,9,11)])
# 9: Organism, 11: Population, excluded since SraRunTable_mrna.txt have both information
df <- data.frame(pData(phenoData(gse[[1]]))[c(1,2)])
colnames(df) <- c('ID','Sample.Name')
sra <- read.csv(file = 'SraRunTable_mrna.txt', sep=',')
merged <- merge(df,sra, by='Sample.Name')

vars <- c("Sample.Name", "ID", "Run","Bases","BioSample","Experiment","Organism","population")
merged <- merged[vars]

write.csv(merged,"Fundulus_mrna_geo.csv", row.names = FALSE, quote = FALSE)
