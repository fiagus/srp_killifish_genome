# Snakefile run order

##### Don't need to run this snakefile:
- metadata.snake: Used to compile all the samples together to make the fully completed sample infos file, the dl.snake will automatically be using this file.

##### Run by order:
Due to the massive amount of samples and jobs, many snakefiles will be needed. If there's too many jobs to run, snakefile takes too long to compile the DAG and will crash.

1. dl.snake: Downloads all SRR samples
2. qual_check.snake: FastQC, MultiQC
3. alignment.snake: pre-processing step
4. variants.snake: Get variants for wgs and radseq + population subset 
