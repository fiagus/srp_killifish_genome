# Snakefile
# USE -l mem_total=24G OR -l mem_per_core=8G OR -l cpu_arch=\!bulldozer
# snakemake --nolock --cluster "qsub -P srpfund -cwd -pe omp {threads} -l h_rt=120:00:00 -l mem_per_core=4G -m a" --latency-wait 20 --snakefile exonerate.snake --jobs 10 -np

import os, pandas as pd

localrules: decompress_gzip, fix_gff, concat_gff

workdir: '../'


hum = expand("samples/exonerate/human_gcf/human_{n}_exo_gcf_ch{c}.results",n=list(range(1,9)), c=list(range(1,301)))
hum_fix = sorted([ _.replace('results','gff') for _ in hum ])

zeb = expand("samples/exonerate/zebrafish_gcf/zebrafish_1_exo_gcf_t{tc}ch{qc}.results",tc=list(range(1,11)),qc=list(range(1,301)))
zeb_fix = sorted([ _.replace('results','gff') for _ in zeb ])

spec = {'human': hum_fix, 'zebrafish': zeb_fix}

ruleorder: zeb_exo > exonerate

rule all:
    input:
#        expand("samples/human.{n}.protein.faa", n=list(range(1,9))),
        zeb,
        hum,
        "samples/exonerate/all_zebrafish_exo_gcf.gff",
#        'samples/exonerate/all_hum_exo_gcf.gff'

rule get_files_installed:
    output: "human.files.installed"
    shell:
        "wget https://ftp.ncbi.nlm.nih.gov/refseq/H_sapiens/mRNA_Prot/human.files.installed"

rule get_protein:
    input: "human.files.installed"
    output: "human.protein.filenames"
    shell:
        "awk '{ print $2 }' {input} | grep 'protein.faa.gz' > {output} "

rule download_proteins:
    output: "samples/human.{n}.protein.faa.gz"
    shell:
        "wget -O https://ftp.ncbi.nlm.nih.gov/refseq/H_sapiens/mRNA_Prot/human.{wildcards.n}.protein.faa.gz {output}"

rule decompress_gzip:
    output: "samples/human.{n}.protein.faa"
    shell:
        "gunzip samples/human.{wildcards.n}.protein.faa.gz"

rule get_zebrafish:
    output: "samples/zebrafish.1.protein.faa.gz"
    shell:
        "wget -O https://ftp.ncbi.nlm.nih.gov/refseq/D_rerio/mRNA_Prot/zebrafish.1.protein.faa.gz samples/zebrafish.1.protein.faa.gz"

rule gz_zebrafish:
    output: "samples/zebrafish.1.protein.faa"
    shell:
        "gunzip samples/zebrafish.1.protein.faa.gz"

rule exonerate:
    input:
        qry = "samples/{species}.{n}.protein.faa",
        killi = "reference/ncbi_genome_2020_10_23/GCF_011125445.2_MU-UCD_Fhet_4.1_genomic.fna"
    output: "samples/exonerate/{species}_gcf/{species}_{n}_exo_gcf_ch{c}.results"
    shell:
        "exonerate --model protein2genome {input.qry} {input.killi} "
        "--showtargetgff yes --showvulgar no --showalignment no --percent 70 "
        "--targetchunkid {wildcards.c} --targetchunktotal 300 > {output[0]}"

rule zeb_exo:
    input:
        qry = "samples/zebrafish.1.protein.faa",
        killi = "reference/ncbi_genome_2020_10_23/GCF_011125445.2_MU-UCD_Fhet_4.1_genomic.fna"
    output: "samples/exonerate/zebrafish_gcf/zebrafish_1_exo_gcf_t{tc}ch{qc}.results"
    shell:
        "exonerate --model protein2genome {input.qry} {input.killi} "
        "--showtargetgff yes --showvulgar no --showalignment no --percent 70 "
        "--querychunkid {wildcards.tc} --querychunktotal 10 "
        "--targetchunkid {wildcards.qc} --targetchunktotal 300 > {output[0]}"

rule fix_gff:
    input: "samples/exonerate/{species}_gcf/{species}_{n}_exo_gcf_{c}.results"
    output: temp("samples/exonerate/{species}_gcf/{species}_{n}_exo_gcf_{c}.gff")
    shell:
        "python fix_exonerate_gff.py {input[0]} > {output[0]}"

rule concat_gff:
    output: "samples/exonerate/all_{species}_exo_gcf.gff"
    params: spec = lambda w: spec[w.species]
    shell:
        "cat {params.spec} > {output[0]}"

