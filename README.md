# README #

Killifish genome project

### Downloading SRR ###

* Get google spreadsheet from Slack
* Use SRS to get SRR and download using SRA tools

### Alignment pipeline ###

* FastQC
* MultiQC
* Trimmomatic -> TruSeq Adapters
* BWA-mem with killifish genome
* FastQC on trimmed paired and BWA files

