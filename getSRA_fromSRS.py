import os
from subprocess import Popen
import pandas as pd

fl = pd.read_csv('Fundulus_metadata.csv', sep=',')
fl = fl[~fl['SRA'].isnull()]
srs_num = ','.join(map(str, fl['SRA'].tolist()))

p = Popen(
        './getSraRunsFromAccIds.py --identifiers {} --outStub srr'.format(srs_num)
        ,shell=True
        )

p.communicate()
