conda install -c conda-forge -c bioconda samtools fastqc bwa star trimmomatic vcftools bcftools ucsc-bigwigmerge ucsc-bedgraphtobigwig freebayes
pip install -r requirements.txt
