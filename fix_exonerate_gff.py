from collections import Counter
import csv
import re
import sys
import zlib

fn = sys.argv[1]

out_f = csv.writer(sys.stdout,delimiter='\t')

def baseN(num,numerals="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOP"):
    b = len(numerals)
    return ((num == 0) and numerals[0]) or (baseN(num // b, numerals).lstrip(numerals[0]) + numerals[num % b])

with open(fn) as f :

    # get rid of Command line and Hostname line
    next(f)
    next(f)

    curr_recs = []
    for rec in csv.reader(f,delimiter='\t') :

        if rec[0].startswith('# --- END OF GFF DUMP ---') :

            gene_rec = curr_recs[0]
            m = re.search('sequence ([^ ]*) ;',gene_rec[-1])
            assert m is not None
            # mRNA feature type enable nesting of exons in jbrowse
            gene_rec[2] = 'mRNA'

            gene_id = m.group(1)

            gff_id = ''.join([gene_id,baseN(int(gene_rec[3])),baseN(int(gene_rec[4]))])

            # fix the gene_id attribute in the gene record
            attrs = gene_rec[-1].split(';')
            attrs = [_.strip().replace(' ','=',1) for _ in attrs if len(_) > 0]

            # filter out genes with similarity < X
            sim_limit = 70
            sim_attr = [_ for _ in attrs if _.startswith('similarity')][0]
            _, sim = sim_attr.split('=')
            if float(sim) < sim_limit :
                curr_recs = []
                continue

            attrs[0] = 'ID='+gff_id

            # get rid of some of the less useful attributes
            blacklist = 'insertion', 'deletion', 'gene_orientation'
            attrs_filt = []
            for attr in attrs :
                if not any([_ in attr for _ in blacklist]) :
                    attrs_filt.append(attr)

            gene_rec[-1] = ';'.join(attrs_filt)
            out_f.writerow(gene_rec)

            # add the gene_id attribute to all the other records
            for r in curr_recs[1:] :
                attrs = r[-1].split(';')
                attrs = [_.strip().replace(' ','=',1) for _ in attrs if len(_) > 0]

                attrs_filt = []
                for attr in attrs :
                    if not any([_ in attr for _ in blacklist]) :
                        attrs_filt.append(attr)

                r = r[:-1]+['Parent={};'.format(gff_id)+';'.join(attrs_filt)]
                out_f.writerow(r)

            curr_recs = []
        # because yea, thanks for making the last line of the file different
        # and invalid, exonerate
        elif rec[0].startswith('--') : 
            continue
        elif not rec[0].startswith('#') :
            rec[1] = '.'
            try :
                if rec[2] not in ('cds','splice5','intron','splice3','similarity') :
                    curr_recs.append(rec)
            except :
                sys.stderr.write(repr(rec)+'\n')
                raise

