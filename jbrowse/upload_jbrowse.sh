#!/bin/bash

ARGS="--acl public-read"

# directories
aws s3 sync $ARGS --exclude ".git/*" jbrowse/plugins s3://$1/plugins
aws s3 sync $ARGS jbrowse/img s3://$1/img
aws s3 sync $ARGS jbrowse/dist s3://$1/dist

# individual files
aws s3 cp $ARGS jbrowse/jbrowse.conf s3://$1/
aws s3 cp $ARGS jbrowse/jbrowse_conf.json s3://$1/
aws s3 cp $ARGS jbrowse/index.html s3://$1/

# also data/trackList.json, since it is updated frequently
aws s3 cp $ARGS jbrowse/data/trackList.json s3://$1/data/

aws s3 sync $ARGS jbrowse/data s3://$1/data
