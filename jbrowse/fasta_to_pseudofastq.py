import sys

fn = sys.argv[1]

with open(fn) as f :
    lines = []
    for r in f :
        if r.startswith('>') :
            if len(lines) > 0 :
                seq = ''.join(lines)
                sys.stdout.write(seq+'\n')
                sys.stdout.write('+\n')
                sys.stdout.write('I'*len(seq)+'\n')
                lines = []
            sys.stdout.write(r.replace('>','@'))
        else :
            lines.append(r.strip())
