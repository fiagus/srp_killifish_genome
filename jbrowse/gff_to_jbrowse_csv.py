
#CM022119.1      nucleotide      mRNA    29012271        29012359        .       +       .       ID=NM_001309983.1;Name=arnt2;Alias=;accession=NM_001309983.1;db_xrefid=GeneID:105921182;gene_desc=aryl-hydrocarbon receptor nuclear translocator 

import csv
import sys

gff = sys.argv[1]

fieldnames = [
    'chrm',
    'start',
    'end',
    'coord',
    'strand',
    'accession',
    'Name',
    'Alias',
    'db_xrefid',
    'gene_desc'
]

if __name__ == '__main__' :
    with open(gff) as f :
        outf = csv.DictWriter(sys.stdout,fieldnames=fieldnames,extrasaction='ignore')
        outf.writeheader()
        for r in csv.reader(f,delimiter='\t') :
            if r[0].startswith('#') :
                continue
            if r[2] == 'mRNA' :
                attrs = r[-1].strip().split(';')
                attrs = [tuple(_.split('=',1)) for _ in attrs]
                # some gene descriptions have ';' characters, causing the dict
                # creation to fail, filter out 1-tuples
                attrs = dict(_ for _ in attrs if len(_) == 2)

                # the computationally inferred sequence descriptions aren't informative
                if attrs['gene_desc'].startswith('Derived') or attrs['gene_desc'].startswith('The sequence'):
                    attrs['gene_desc'] = ''

                rec = {
                        'chrm':r[0],
                        'start':r[3],
                        'end':r[4],
                        'coord':'{}:{}..{}'.format(r[0],r[3],r[4]),
                        'strand':r[6],
                }
                rec.update(attrs)

                outf.writerow(rec)

