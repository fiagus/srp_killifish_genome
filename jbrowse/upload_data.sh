#!/bin/bash

ARGS="--acl public-read"
aws s3 sync $ARGS jbrowse/data s3://$1/data
