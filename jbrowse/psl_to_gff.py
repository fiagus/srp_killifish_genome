import csv
from collections import defaultdict, Counter
import logging
import sys

logging.basicConfig(level=logging.INFO)

psl_fn = sys.argv[1]
annot_fn = sys.argv[2]

fields = [
    'match',
    'mismatch',
    'repmatch',
    'Ns',
    'Qgapcount',
    'Qgapbases',
    'Tgapcount',
    'Tgapbases',
    'strand',
    'Qname',
    'Qsize',
    'Qstart',
    'Qend',
    'Tname',
    'Tsize',
    'Tstart',
    'Tend',
    'blockcount',
    'blockSizes',
    'qStarts',
    'tStarts'
]


annot = {}
with open(annot_fn) as f :
    for r in csv.DictReader(f,fieldnames=['acc','gene','gene_synonym','db_xrefid','gene_desc']) :
        annot[r['acc']] = r

logging.info('{} annotations loaded'.format(len(annot)))

stats = Counter()

with open(psl_fn) as f :
    f = csv.DictReader(f,delimiter='\t',fieldnames=fields)
    queries = defaultdict(list)
    for r in f :

        if r['match'].startswith('psLayout') :
            # get rid of top matter
            while not r['match'].startswith('--') :
                r = next(f)
            r = next(f)

        if r['Tname'] is None :
            logging.warning(r)
        #if r['Tname'].startswith('CM') :
        queries[r['Qname']].append(r)

logging.info('{} queries aligned'.format(len(queries)))

# seqid
# source
# type
# start
# end
# score
# strand
# phase
# attributes
out_f = sys.stdout
out_f.write('##gff-version 3\n')
num_unannot = 0
for query, aligns in queries.items() :

    best_align = max(aligns, key=lambda r: int(r['match'])-int(r['mismatch']))

    if best_align['Qname'] not in annot :
        #logging.info('missing transcript: {}'.format(best_align['Qname']))
        num_unannot += 1
        continue

    aliases = []
    synonyms = annot[best_align['Qname']]['gene_synonym']
    if synonyms != '' :
        aliases = synonyms.split(',')
    aliases.append(best_align['Qname'])
    aliases = ','.join([_ for _ in aliases if len(_) != 0])

    out_f.write('\t'.join([
            best_align['Tname'],
            'nucleotide',
            'mRNA',
            best_align['Tstart'],
            best_align['Tend'],
            '.',
            best_align['strand'],
            '.',
            'ID={acc};Name={gene};Alias={gene_synonym};accession={acc};db_xrefid={db_xrefid};gene_desc={gene_desc}'.format(**annot.get(best_align['Qname'],{}))
        ])+'\n'
    )
    starts = best_align['tStarts'].split(',')
    sizes = best_align['blockSizes'].split(',')

    for start,size in zip(starts, sizes) :
        if start != '' :
            out_f.write('\t'.join([
                    best_align['Tname'],
                    'nucleotide',
                    'exon',
                    start,
                    str(int(start)+int(size)),
                    '.',
                    best_align['strand'],
                    '.',
                    'Parent={acc}'.format(**annot.get(best_align['Qname'],{}))
                ])+'\n'
            )

logging.info('Total missing annotation: {}'.format(num_unannot))
